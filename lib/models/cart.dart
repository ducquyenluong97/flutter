import 'package:flutter/foundation.dart';

class Product {
  String name;
  double price;
  String id;
  Product(this.id, this.name, this.price);
  
}
class Item {
  String productID;
  Product product;
  int quantity;
  double total;
  Item(this.productID, this.quantity, this.total, this.product);
}

class Cart extends ChangeNotifier {
  List<Item> _items = [];
  List<Item> get items => _items;
  void createItem(product) {
    Item item = new Item(product.id, 1, 0, product);
    _items.add(item);
  }

  double totalPrice() {
    return _items.fold(0, (prev, element) => prev + element.product.price * element.quantity);
  }  

  void subQuantity(product) {
    print('name: ${product.name}');
    for (int i = 0; i < _items.length; i++) {
      if (_items[i].productID == product.id) {
        if (_items[i].quantity == 1) {
          _items.removeWhere((item) => item.productID == product.id);
        } else {
          _items[i].quantity = _items[i].quantity - 1;
        }
      }
    }
    notifyListeners();
  }

  void updateQuantity(product) {
    print('product name ${product.name}');
     //this.createItem(product);
    var check = _items.firstWhere((item) => item.productID == product.id,
        orElse: () => null);
      print('check $check');
    if (check == null) {
      this.createItem(product);
    } else {
      for (int i = 0; i < _items.length; i++) {
        if (_items[i].productID == product.id) {
          _items[i].quantity = _items[i].quantity + 1;
        }
      }
    }
    print(_items.length);
    notifyListeners();
  }
  
}
