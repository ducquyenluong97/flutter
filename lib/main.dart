import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:state_management/models/cart.dart';

void main() => runApp(
    ChangeNotifierProvider(builder: (context) => Cart(), child: MyApp()));

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      initialRoute: '/',
      routes: {'/cart': (context) => CartScreen()},
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: ListProduct(
        items: List.generate(9, (i) => Product('$i', 'Television $i', 12)),
      ),
    );
  }
}

class CartScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var cart = Provider.of<Cart>(context);
    return Scaffold(
        appBar: AppBar(
          title: Text('My Cart'),
        ),
        body: Container(
          child: Column(
            children: <Widget>[
              Expanded(
                child: ListView.builder(
                  itemCount: cart.items.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                      title: Text(cart.items[index].product.name + ' - Price: ' + cart.items[index].product.price.toString(),
                          style: TextStyle(fontSize: 14.0)),
                      subtitle: Text('Quantity: ' + cart.items[index].quantity.toString()),
                      trailing: Container(
                        child: RaisedButton(
                          onPressed: () {
                            cart.subQuantity(cart.items[index].product);
                          },
                          child: Text('-'),
                          
                        ),
                      ),
                    );
                  },
                ),
              ),
              Divider(height: 4, color: Colors.black,),
              _CartTotal()
            ],
          ),
        ));
  }
}

class ListProduct extends StatelessWidget {
  final List<Product> items;
  ListProduct({Key key, @required this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var cart = Provider.of<Cart>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("List Product"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.list),
            onPressed: () => Navigator.pushNamed(context, '/cart'),
          )
        ],
      ),
      body: ListView.builder(
        itemCount: items.length,
        itemBuilder: (context, index) {
          return ListTile(
            onTap: () {},
            title: Text(items[index].name, style: TextStyle(fontSize: 14.0)),
            trailing: RaisedButton(
              onPressed: () {
                cart.updateQuantity(items[index]);
              },
              child: Text('Add to cart'),
            ),
          );
        },
      ),
    );
  }
}

class _CartTotal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var hugeStyle = Theme.of(context).textTheme.display4.copyWith(fontSize: 48);
    return SizedBox(
      height: 200,
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Consumer<Cart>(
                builder: (context, cart, child) =>
                    Text('\$${cart.totalPrice()}', style: hugeStyle)),
            SizedBox(width: 24),
            FlatButton(
              onPressed: () {
                Scaffold.of(context).showSnackBar(
                    SnackBar(content: Text('Buying not supported yet.')));
              },
              color: Colors.white,
              child: Text('BUY'),
            ),
          ],
        ),
      ),
    );
  }
}
